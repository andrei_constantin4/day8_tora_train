package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T>{
    private List<T> list;

    public ArrayListBasedRepository() {
        this.list = new ArrayList<>();
    }

    public ArrayListBasedRepository(T[] t) {
        this.list = new ArrayList<>(Arrays.asList(t));
    }

    @Override
    public void add(T t) {
        list.add(t);
    }

    @Override
    public boolean contains(T t) {
        return list.contains(t);
    }

    @Override
    public void remove(T t) {
        list.remove(t);
    }
}
