package models;

import java.util.Objects;

public class Order implements Comparable<Order>{
    private int id;
    private int price;
    private int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id && price == order.price && quantity == order.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price, quantity);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public int compareTo(Order o) {
        if(id<o.id){
            return -1;
        }else if(id==o.id){
            if(price<o.price){
                return -1;
            }else if(price==o.price){
                if(quantity<o.quantity){
                    return -1;
                }else if(quantity==o.quantity){
                    return 0;
                }
                return 1;
            }
            return 1;
        }
        return 1;
    }
}
