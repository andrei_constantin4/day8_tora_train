package models;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T>{
    Set<T> set;

    public TreeSetBasedRepository() {
        set=new TreeSet<>();
    }

    public TreeSetBasedRepository(T[] t) {
        set=new TreeSet<>(Arrays.asList(t));
    }

    @Override
    public void add(T t) {
        set.add(t);
    }

    @Override
    public boolean contains(T t) {
        return set.contains(t);
    }

    @Override
    public void remove(T t) {
        set.remove(t);
    }
}
