/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.tora;

import models.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Random;

public class MyBenchmark {

    /*@State(Scope.Benchmark)
    public static class Plan {
        public Order[] arr=new Order[N];
        @Setup(Level.Iteration)
        public void setUp() {
            for(int i=0;i<N;i++){
                arr[i]=getRandomOrder();
            }
        }
    }

    @State(Scope.Benchmark)
    public static class PlanContains {
        public ArrayListBasedRepository<Order>list=new ArrayListBasedRepository<>();
        public HashSetBasedRepository<Order>hashSet=new HashSetBasedRepository<>();
        public TreeSetBasedRepository<Order>treeSet=new TreeSetBasedRepository<>();
        public Order[] searched=new Order[N];
        public Order[] orders=new Order[N];
        @Setup(Level.Iteration)
        public void setUp() {
            for(int i=0;i<N;i++){
                Order o=getRandomOrder();
                orders[i]=o;
                list.add(o);
                hashSet.add(o);
                treeSet.add(o);
            }
            for(int i=0;i<N;i++){
                int currentPos=(int)(Math.random() * ((N-1) + 1));
                searched[i]=orders[currentPos];
            }
        }
    }

    @State(Scope.Benchmark)
    public static class PlanRemove {
        public ArrayListBasedRepository<Order>list=new ArrayListBasedRepository<>();
        public HashSetBasedRepository<Order>hashSet=new HashSetBasedRepository<>();
        public TreeSetBasedRepository<Order>treeSet=new TreeSetBasedRepository<>();
        public Order[] searched=new Order[N];
        public Order[] orders=new Order[N];
        @Setup(Level.Iteration)
        public void setUp() {
            for(int i=0;i<N;i++){
                Order o=getRandomOrder();
                orders[i]=o;
                list.add(o);
                hashSet.add(o);
                treeSet.add(o);
            }
            for(int i=0;i<N;i++){
                int currentPos=(int)(Math.random() * ((N-1) + 1));
                searched[i]=orders[currentPos];
            }
        }
    }*/
    @State(Scope.Benchmark)
    public static class Plan {
        public ArrayListBasedRepository<Order>list=new ArrayListBasedRepository<>();
        public HashSetBasedRepository<Order>hashSet=new HashSetBasedRepository<>();
        public TreeSetBasedRepository<Order>treeSet=new TreeSetBasedRepository<>();
        public Order[] searched=new Order[N];
        public Order[] orders=new Order[N];
        @Setup(Level.Iteration)
        public void setUp() {
            for(int i=0;i<N;i++){
                Order o=getRandomOrder();
                orders[i]=o;
                list.add(o);
                hashSet.add(o);
                treeSet.add(o);
            }
            for(int i=0;i<N;i++){
                int currentPos=(int)(Math.random() * ((N-1) + 1));
                searched[i]=orders[currentPos];
            }
        }
    }

    private static final int N=10000;

    public static Order getRandomOrder(){
        Random random=new Random();
        return new Order(random.nextInt(), random.nextInt(), random.nextInt());
    }
    @Benchmark
    public void testAddArrayList(Plan plan) {
        ArrayListBasedRepository<Order>list=new ArrayListBasedRepository<>();
        for(int i=0;i<N;i++){
            list.add(plan.orders[i]);
        }
    }

    @Benchmark
    public void testContainsArrayList(Plan plan) {
        for(int i=0;i<N;i++){
            plan.list.contains(plan.searched[i]);
        }
    }

    @Benchmark
    public void testRemoveArrayList(Plan plan) {
        for(int i=0;i<N;i++){
            plan.list.remove(plan.searched[i]);
        }
    }

    @Benchmark
    public void testAddHashSet(Plan plan) {
        HashSetBasedRepository<Order>hashSet=new HashSetBasedRepository<>();
        for(int i=0;i<N;i++){
            hashSet.add(plan.orders[i]);
        }
    }

    @Benchmark
    public void testContainsHashSet(Plan plan) {
        for(int i=0;i<N;i++){
            plan.hashSet.contains(plan.searched[i]);
        }
    }

    @Benchmark
    public void testRemoveHashSet(Plan plan) {
        for(int i=0;i<N;i++){
            plan.hashSet.remove(plan.searched[i]);
        }
    }

    @Benchmark
    public void testAddTreeSet(Plan plan) {
        TreeSetBasedRepository<Order>treeSet=new TreeSetBasedRepository<>();
        for(int i=0;i<N;i++){
            treeSet.add(plan.orders[i]);
        }
    }
    @Benchmark
    public void testContainsTreeSet(Plan plan) {
        for(int i=0;i<N;i++){
            plan.treeSet.contains(plan.searched[i]);
        }
    }

    @Benchmark
    public void testRemoveTreeSet(Plan plan) {
        for(int i=0;i<N;i++){
            plan.treeSet.remove(plan.searched[i]);
        }
    }

    public static void main(String[] args) throws RunnerException {

        Options opt = new OptionsBuilder()
                .include(MyBenchmark.class.getSimpleName())
                .warmupIterations(5)
                .measurementIterations(2)
                .forks(1)  //a new java process for each trial
                .build();

        new Runner(opt).run();
    }
}
